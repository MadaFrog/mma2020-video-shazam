import pickle
import glob
import sys
from features import *

'''
This script recreates the 'video_db.pkl' file which contains the feature arrays of the trainings videos (the videos in
the 'Videos' folder). So it only needs to be executed if the training set is updated, or if there is no other way to get
the pkl file, because it takes a while to create the database.
'''

# grab all video file names
video_types = ('*.mp4', '*.avi')
video_list = []
training_features = []
for type_ in video_types:
    files = 'Videos/' + type_
    video_list.extend(glob.glob(files))
video_list.sort()

# loop over all videos in the database and compare frame by frame
for video in video_list:
    print video
    training_features.append(get_features(to_array(video, sys.maxint)))

with open('video_db.pkl', 'wb') as fi:
    pickle.dump(training_features, fi)
