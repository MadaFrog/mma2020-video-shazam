import cv2
import numpy as np


# convert a video with any frame rate to an array of frames
def to_array(video, max_sec):
    array = []
    vc = cv2.VideoCapture(video)
    fps = vc.get(cv2.CAP_PROP_FPS)

    # get frames of the whole video or up until the given ending timestamp
    while vc.isOpened() and (len(array) / fps) < max_sec:
        ret, frame = vc.read()
        if frame is None:
            break
        width = len(frame[0])
        height = len(frame)
        if height == 0 or width == 0:
            break
        if width < height:
            frame = np.rot90(frame, 1)
        array.append(frame)

    vc.release()

    if int(round(fps)) == 30:
        return array
    else:
        i = 0.0
        result = []
        while int(round(i)) < len(array):
            result.append(array[int(round(i))])
            i += fps / 30.0
        print len(result)
        return result


# create 64 segments out of a frame
def segment(frame):
    s = []
    w = frame.shape[1]
    h = frame.shape[0]
    for i in range(8):
        for j in range(8):
            s.append(np.mean(frame[int(i * h * 0.125):int((i + 1) * h * 0.125),
                             int(j * w * 0.125):int((j + 1) * w * 0.125)], axis=(0, 1)))
    return s


# get the feature array between two frames
def average_diffs(frame1, frame2):
    if frame1 is None or frame2 is None:
        return None
    averages1 = segment(frame1)
    averages2 = segment(frame2)
    d = np.sum(abs(np.subtract(averages1, averages2)))
    return d


# get the array of the features of a video
def get_features(frames):
    features = []
    prev_frame = None
    frame_nbr = 0

    for frame in frames:
        if frame is None:
            break
        feature = average_diffs(prev_frame, frame)
        if feature is not None:
            features.append(feature)
        prev_frame = frame
        frame_nbr += 1

    return features
