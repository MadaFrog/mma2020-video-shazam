import pickle
import glob
import sys
from features import *
from crop import *


# compare two feature arrays by sliding them along each other
def sliding_window(q_features, m_features):
    while len(q_features) >= len(m_features):
        q_features = q_features[:int(0.75 * len(q_features))]
    minimum = sys.maxint
    frame_number = 0
    min_frame = 0
    diffs = []

    for i in range(len(m_features) - len(q_features)):
        diff = np.linalg.norm(q_features - m_features[i:(i + len(q_features))])
        diffs.append(diff)

        if diff < minimum:
            minimum = diff
            min_frame = frame_number
        frame_number += 1

    return min_frame / 30, minimum


# sort ranking
def insertion_sort(ra, s, t):
    for ss in range(1, len(s)):
        e = s[ss]
        li = ra[ss]
        g = t[ss]
        j = ss - 1
        while j >= 0 and e < s[j]:
            s[j + 1] = s[j]
            ra[j + 1] = ra[j]
            t[j + 1] = t[j]
            j -= 1
        s[j + 1] = e
        ra[j + 1] = li
        t[j + 1] = g


# grab all video file names
video_types = ('*.mp4', '*.avi')
query_list = []
for type_ in video_types:
    files = 'Queries/' + type_
    query_list.extend(glob.glob(files))

# calculate results for each video in the 'Queries' folder
for query in query_list:
    print "\n---------------------------------------------------------------------------------------------------"
    print '\nCropping ' + query + '...\n'
    vid = crop_video(query)
    print 'Results for ' + query + ':\n'

    # grab all video file names
    video_list = []
    for type_ in video_types:
        files = 'Videos/' + type_
        video_list.extend(glob.glob(files))
    video_list.sort()
    db = []
    with open('video_db.pkl', 'rb') as fi:
        db = pickle.load(fi)

    # get the top 5 best matches
    ranking = []
    scores = []
    seconds = []
    best_match = video_list[0]
    query_features = np.asarray(get_features(vid))

    # loop over all videos in the database and compare frame by frame
    for v, video in enumerate(video_list):
        sec, min_diff = sliding_window(query_features, np.asarray(db[v]))
        ranking.append(video)
        scores.append(min_diff)
        seconds.append(sec)

    # print the top 5 best matches
    print '\nTop 5 matches:\n'
    insertion_sort(ranking, scores, seconds)
    for r in range(5):
        print str(r + 1) + '. ' + ranking[r] + '\nWith a difference factor of: ' + str(int(scores[r])) + \
              ' - Starting at second: ' + str(seconds[r]) + '\n'
    print "---------------------------------------------------------------------------------------------------\n"
