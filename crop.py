import cv2
import numpy as np
from features import *


# fix the angle of the rectangle
def rotate_rectangle(rectangle):
    angle = rectangle[2]
    if rectangle[1][0] < rectangle[1][1]:
        if angle < -45:
            rectangle = (rectangle[0], (rectangle[1][1], rectangle[1][0]), angle + 90)
        if angle > 45:
            rectangle = (rectangle[0], (rectangle[1][1], rectangle[1][0]), angle - 90)

    return rectangle


# get the cropped version of the frame
def crop_frame(frame, rectangle):
    rectangle = rotate_rectangle(rectangle)
    # rotate img
    angle = rectangle[2]
    rows, cols = frame.shape[0], frame.shape[1]
    matrix = cv2.getRotationMatrix2D((cols / 2, rows / 2), angle, 1)
    img_rotated = cv2.warpAffine(frame, matrix, (cols, rows))
    # rotate bounding box
    box = cv2.boxPoints(rectangle)
    pts = np.int0(cv2.transform(np.array([box]), matrix))[0]
    pts[pts < 0] = 0
    return img_rotated[pts[1][1]:pts[0][1], pts[1][0]:pts[2][0]]


# get the rectangle surrounding the moving areas in the video
def get_rectangle(sum_diffs):
    contours, hierarchy = cv2.findContours(sum_diffs, cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)
    best_contour = sorted(contours, key=cv2.contourArea, reverse=True)[0]
    rectangle = cv2.minAreaRect(best_contour)
    box = cv2.boxPoints(rectangle)
    box = np.int0(box)
    cv2.drawContours(sum_diffs, [box], 0, (200, 0, 0), 2)
    return rectangle


# use canny edges in case the movement detection did not work
def get_canny(frames):
    width = len(frames[0][0])
    height = len(frames[0])
    area = width * height

    for f in frames:
        img_edges = cv2.Canny(f, 50, 80)
        img_edges[int(0.9 * height)] = 0
        contours, new = cv2.findContours(img_edges, cv2.RETR_LIST, cv2.CHAIN_APPROX_NONE)

        # as soon as a proper rectangle is found return it
        for contour in contours:
            rectangle = cv2.minAreaRect(contour)
            rect_area = rectangle[1][0] * rectangle[1][1]
            if rect_area > 0.25 * area and rectangle[1][1] * 2 > rectangle[1][0] > rectangle[1][1] * 0.5:
                return rectangle

    return None


# crop the video
def crop_video(video):
    result = []
    frames = to_array(video, 30)
    area = len(frames[0]) * len(frames[0][0])
    prev_frame = None
    sum_diffs = []
    threshold = 50

    for frame in frames:
        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

        if prev_frame is None:
            sum_diffs = np.zeros_like(gray)
        else:
            diff = cv2.subtract(frame, prev_frame)
            gray_diff = cv2.cvtColor(diff, cv2.COLOR_BGR2GRAY)
            gray_diff[gray_diff < threshold] = 0
            sum_diffs += gray_diff

        prev_frame = frame

    sum_diffs = cv2.blur(sum_diffs, (15, 15))
    sum_diffs[sum_diffs > threshold] = 255
    if np.mean(sum_diffs) > 225:
        return frames
    rectangle = get_rectangle(sum_diffs)
    rect_area = rectangle[1][0] * rectangle[1][1]

    # not a valid rectangle
    if rect_area < 0.1 * area and rectangle[1][0] > rectangle[1][1] * 2 or \
            rectangle[1][0] < rectangle[1][1] * 0.5:
        canny = get_canny(frames)

        # system gives up cropping
        if canny is None:
            print 'Unable to crop the video out, so the whole frame is being queried...\n'
            return frames
        rectangle = canny

    # valid rectangle
    for frame in frames:
        crop = crop_frame(frame, rectangle)
        result.append(crop)
    cv2.destroyAllWindows()
    return result
