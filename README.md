![Logo](https://i.imgur.com/yEEHGNH.png)

# MMA2020 - Video Shazam

### The required environment for it to run properly
This program is created and tested in Windows, so it may show errors when running it on another operating system.

Make sure you have installed only Python 2.7.9 or above, so uninstall Python 3 or any other Anaconda versions just to make sure they don't interfere.

After installing Python 2.7.X, you might need to add it to PATH. To do this, go to This PC, right click and go to "Properties". Now on the top left side click "Advanced System settings". In the right bottom corner, click "Environment Variables". In the top table, select "Path" and click "Edit...". Now click "New" and add the patch to Python27, which by default should be "C:\Python27" and also add the Scripts folder to be aable to use "pip" which by default is "C:\Python27\Scripts". Now make sure you click OK to close every opened tab.

Open cmd as administrator and run the following commands to install the necessary libraries:
```
pip install opencv-python

pip install opencv-contrib-python
```

Also please run the following command to check the version of "opencv-python" and "opencv-contrib-python" and make sure it's version 4.2.0.32
```
pip list
```

If you’re using VS Code, please go to the plug-ins and make sure python is installed. Also, after doing all of this, when you open VSCode, you should see a pop-up and click install.

Just in case you run the query.py file and you get any errors about the .pkl file, then please download the features database from here [video_db.pkl](https://drive.google.com/file/d/1rnHOZ8_gIqSQ0AnEer30PVILvv-UzJdA/view?usp=sharing) and overwrite it in the "mma2020-video-shazam" folder.

### Input video conditions
Please keep the camera as steady as possible while recording and in front of the screen playing the video, with as less perspective change as possible (keep your phone parallel to the screen such that the camera is pointing straight towards the screen)

The longer the video (up to 30s), the better the results.

The screen playing the video has to cover at least a quarter of the video given as input to the system.

## How to run it
Add the videos you want to "Shazam" in the "Queries" folder and run the query.py file. Then the code will output the results for each video in the folder, displaying the top 5 best matches for each of them.

## What works
Our system can handle in-plane rotation, as long as it doesn't change during the query video.
The system can handle any video frame rate.
Most of the time, poor lighting conditions can be handled.

## What doesn't work
It doesn't work as expected if there is not too much movement in the video, because we do our cropping based on how much the video changes. We also have a back-up plan where we try to crop the video using edge detection, but that is not very reliable unless there is clear contrast between the frame of the screen and the background or between the screen itself and the window where the video is playing.

### Why it doesn't work
If there is not a lot of changing pixels in the video, then the cropping fails, if the edge detection also fails, then we use the whole video which leads to a lot less accurate matching.